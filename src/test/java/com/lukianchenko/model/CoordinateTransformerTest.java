package com.lukianchenko.model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class CoordinateTransformerTest {
    double a = 6378137;
    double f = 0.003352810681182319;
    double x = 3194469.145086824;
    double y = 3194469.1450868235;
    double z = 4487419.119432919;

    DataTransferObject dataTransferObject = new DataTransferObject();
    public void coordinatesLoading() {
        dataTransferObject.setCoordinateXorB(x);
        dataTransferObject.setCoordinateYorL(y);
        dataTransferObject.setCoordinateZorH(z);
        dataTransferObject.setSemiMajorAxis(a);
        dataTransferObject.setFlatting(f);
        dataTransferObject.setOption(1);
    }


    @Test
    public void testTransform() {
        CoordinateTransformer coordinateTransformer = new CoordinateTransformer();
        DataTransferObject dataTransferObjectIn = dataTransferObject;
        coordinatesLoading();
        for(int i = 0; i < 100; i++) {
            dataTransferObjectIn.setOption(1);
            DataTransferObject dataTransferObjectOut = coordinateTransformer.transform(dataTransferObjectIn);
            dataTransferObjectOut.setOption(2);
            dataTransferObjectIn = coordinateTransformer.transform(dataTransferObjectOut);
        }
        double dx = dataTransferObjectIn.getCoordinateXorB() - 3194469.145086824;
        double dy = dataTransferObjectIn.getCoordinateYorL() - 3194469.1450868235;
        double dz = dataTransferObjectIn.getCoordinateZorH() - 4487419.119432919;

        assertTrue(dx < 1*10E-8);
        assertTrue(dy < 1*10E-8);
        assertTrue(dz < 1*10E-8);
    }

}
