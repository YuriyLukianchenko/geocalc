package com.lukianchenko.model;

public class CoordinateTransformer {

    public DataTransferObject transform (DataTransferObject dataTransferObject) {
        DataTransferObject dataTransferObjectOut;
        if (dataTransferObject.getOption() == 1) {
            dataTransferObjectOut = xyztoblh(dataTransferObject);
        } else {
            dataTransferObjectOut = blhtoxyz(dataTransferObject);
        }
    return dataTransferObjectOut;
    }

    private DataTransferObject blhtoxyz (DataTransferObject dataTransferObject) {
        double latitude = dataTransferObject.getCoordinateXorB();
        double longitude = dataTransferObject.getCoordinateYorL();
        double height = dataTransferObject.getCoordinateZorH();
        double semiMajorZxis = dataTransferObject.getSemiMajorAxis();
        double flatting = dataTransferObject.getFlatting();

        double semiMinorAxis = semiMajorZxis*(1 - flatting);
        double squareFirstExcentrisitet = (semiMajorZxis * semiMajorZxis - semiMinorAxis * semiMinorAxis) / semiMajorZxis / semiMajorZxis;

        //from degrees to radians
        latitude = Math.PI / 180 * latitude;
        longitude = Math.PI / 180 * longitude;


        double radiusOfFirstVertical = semiMajorZxis / Math.sqrt(1 - squareFirstExcentrisitet * Math.pow(Math.sin(latitude),2));
        double X = (radiusOfFirstVertical +height) * Math.cos(latitude)*Math.cos(longitude);
        double Y = (radiusOfFirstVertical +height) * Math.cos(latitude)*Math.sin(longitude);
        double Z = (radiusOfFirstVertical * (1 - squareFirstExcentrisitet) + height) * Math.sin(latitude);

        DataTransferObject dataTransferObjectOut = new DataTransferObject();
        dataTransferObjectOut.setFlatting(flatting);
        dataTransferObjectOut.setSemiMajorAxis(semiMajorZxis);
        dataTransferObjectOut.setCoordinateXorB(X);
        dataTransferObjectOut.setCoordinateYorL(Y);
        dataTransferObjectOut.setCoordinateZorH(Z);
        return dataTransferObjectOut;
    }

    private DataTransferObject xyztoblh (DataTransferObject dataTransferObject) {
        double x = dataTransferObject.getCoordinateXorB();
        double y = dataTransferObject.getCoordinateYorL();
        double z = dataTransferObject.getCoordinateZorH();
        double semiMajorZxis = dataTransferObject.getSemiMajorAxis();
        double flatting = dataTransferObject.getFlatting();

        double semiMinorAxis = semiMajorZxis*(1 - flatting);
        double squareFirstExcentrisitet = (semiMajorZxis * semiMajorZxis - semiMinorAxis * semiMinorAxis) / semiMajorZxis / semiMajorZxis;

        double longitude = Math.atan2(x, y) * 180 / (Math.PI);
        double l = longitude;
        // just short marks
        double r, e22, E2, F, G, C, S, P, Q, r0, U, V, z0;
        double e2 = squareFirstExcentrisitet;
        double bb = semiMinorAxis;
        double a = semiMajorZxis;
        double f = flatting;
        double h, b;
        r = Math.sqrt(x*x+y*y);
        //System.out.println(r);
        //console.log(r);
        e22 = (a*a-bb*bb)/(bb*bb);
        //console.log(e22);
        //System.out.println(e22);
        E2 = a*a-bb*bb;
        //console.log(E2);
        //System.out.println(E2);
        F = 54*bb*bb*z*z;
        //console.log(F);
        //System.out.println(F);
        G = r*r+(1-e2)*z*z-e2*E2;
        //console.log(G);
        //System.out.println(G);
        C = e2*e2*F*r*r/(G*G*G);
        //console.log(C);
        //System.out.println(C);
        S = Math.pow(1+C+Math.sqrt(C*C+2*C),1.0/3.0);
        //console.log(S);
        //System.out.println(S);
        P = F/(3*G*G*(S+1+1/S)*(S+1+1/S));
        //console.log(P);
        //System.out.println("P=" + P);
        Q = Math.sqrt(1+2*e2*e2*P);
        //console.log(Q);
        //System.out.println(Q);
        r0 = -(P*e2*r)/(1+Q)+Math.sqrt(0.5*a*a*(1+1/Q)-(P*z*z*(1-e2))/(Q*(1+Q))-0.5*P*r*r);
        //console.log(r0);
        //System.out.println("r0= " + r0);
        U = Math.sqrt((r-e2*r0)*(r-e2*r0)+z*z);
        //console.log(U);
        //System.out.println(U);
        V = Math.sqrt((r-e2*r0)*(r-e2*r0)+(1-e2)*z*z);
        //System.out.println(V);
        //System.out.println("----");

        z0 = bb*bb*z/a/V;

        h = U*(1-bb*bb/a/V);

        b = Math.atan((z+e22*z0)/r);
        b = b*180/(Math.PI);

        DataTransferObject dataTransferObjectOut = new DataTransferObject();
        dataTransferObjectOut.setFlatting(flatting);
        dataTransferObjectOut.setSemiMajorAxis(semiMajorZxis);
        dataTransferObjectOut.setCoordinateXorB(b);
        dataTransferObjectOut.setCoordinateYorL(l);
        dataTransferObjectOut.setCoordinateZorH(h);

        return dataTransferObjectOut;
    }
}
