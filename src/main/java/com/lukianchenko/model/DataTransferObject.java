package com.lukianchenko.model;

/**
 * @Author Yuriy
 * Stores three abstract coordinates (BLH or XYZ)
 */
public class DataTransferObject {
    private double coordinateXorB;
    private double coordinateYorL;
    private double coordinateZorH;
    private double semiMajorAxis;
    private double flatting;
    private int option;

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public double getCoordinateXorB() {
        return coordinateXorB;
    }

    public void setCoordinateXorB(double coordinateXorB) {
        this.coordinateXorB = coordinateXorB;
    }

    public double getCoordinateYorL() {
        return coordinateYorL;
    }

    public void setCoordinateYorL(double coordinateYorL) {
        this.coordinateYorL = coordinateYorL;
    }

    public double getCoordinateZorH() {
        return coordinateZorH;
    }

    public void setCoordinateZorH(double coordinateZorH) {
        this.coordinateZorH = coordinateZorH;
    }

    public double getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public void setSemiMajorAxis(double semiMajorAxis) {
        this.semiMajorAxis = semiMajorAxis;
    }

    public double getFlatting() {
        return flatting;
    }

    public void setFlatting(double flatting) {
        this.flatting = flatting;
    }

    @Override
    public String toString() {
        return "DataTransferObject{" +
                "coordinateXorB=" + coordinateXorB +
                ", coordinateYorL=" + coordinateYorL +
                ", coordinateZorH=" + coordinateZorH +
                ", semiMajorAxis=" + semiMajorAxis +
                ", flatting=" + flatting +
                '}';
    }
}
