package com.lukianchenko.exception;

import java.util.InputMismatchException;

public class WrongInputDataFormatException extends InputMismatchException {
}
