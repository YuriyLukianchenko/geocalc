package com.lukianchenko.controller;

import com.lukianchenko.model.CoordinateTransformer;
import com.lukianchenko.model.DataTransferObject;

/**
 * @Author Yuriy
 * Transport data form menu to calculation module
 * option = 1 (XYZ -> BLH)
 * option = 2 (BLH -> XYZ)
 */
public class DataController {
    private CoordinateTransformer coordinateTransformer;

    public DataController(CoordinateTransformer coordinateTransformer) {
        this.coordinateTransformer = coordinateTransformer;
    }
    public DataTransferObject getTransformedCoordinates(DataTransferObject dataTransferObject) {
        return coordinateTransformer.transform(dataTransferObject);
    }
}
