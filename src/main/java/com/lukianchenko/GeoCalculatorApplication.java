package com.lukianchenko;

import com.lukianchenko.controller.DataController;
import com.lukianchenko.model.CoordinateTransformer;
import com.lukianchenko.view.Menu;

public class GeoCalculatorApplication {
    public static void main(String[] args) {
        CoordinateTransformer coordinateTransformer = new CoordinateTransformer();
        DataController dataController = new DataController(coordinateTransformer);
        Menu menu = new Menu(dataController);

        menu.start();

    }
}
