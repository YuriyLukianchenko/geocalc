package com.lukianchenko.view;

import com.lukianchenko.controller.DataController;
import com.lukianchenko.exception.WrongInputDataFormatException;
import com.lukianchenko.model.DataTransferObject;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @Author Yuriy
 * Menu for managing  main operations of program
 */
public class Menu {
    private DataController dataController;
    private double majorSemiAxis;
    private double flatting;
    private double firstCoordinate;
    private double secondCoordinate;
    private double thirdCoordinate;
    private int option; // 1 - from XYZ to BLH, 2 - from BLH to XYZ
    private DataTransferObject dataTransferObjectIn = new DataTransferObject();
    private DataTransferObject dataTransferObjectOut = new DataTransferObject();

    Scanner scanner = new Scanner(System.in);
    Scanner ellipsoidScanner = new Scanner(System.in);
    Scanner flattingScanner = new Scanner(System.in);
    Scanner coordinatesScanner = new Scanner(System.in);

    public Menu (DataController dataController) {
        this.dataController = dataController;
    }

    private void showInitialInfo(){
        System.out.println("--- GeoCalculator ---");
        System.out.println("input 1 for XYZ -> BLH conversion");
        System.out.println("input 2 for BLH -> XYZ conversion");
    }
    public void start() {
        int userInput;
        showInitialInfo();
        label:
        try{
            while(true) {
                userInput =  scanner.nextInt();
                switch(userInput){
                    case 1:
                        option = 1;
                        startConversion();
                        break label;
                    case 2:
                        option = 2;
                        startConversion();
                        break label;
                    default:
                        System.out.println("unknown option, input '1' or '2'");
                        continue;
                }
            }
        }
        catch(InputMismatchException error){
            System.out.println("sorry it is not a number, please input '1' or '2'");
            scanner.nextLine();
            start();
        }
    }
    //to do method for uniform massages for BLH and XYZ
    private void startConversion(){
        if (option == 1) {
            System.out.println("Your choice is XYZ -> BLH conversion");
            System.out.println("At first input ellipsoid's parameters ");
            System.out.println("input major semi-axis a [m] = ");
        } else {
            System.out.println("Your choice is BLH -> XYZ conversion");
            System.out.println("At first input ellipsoid's parameters ");
            System.out.println("input major semi-axis a [m] = ");
        }
        try{
            majorAxisScan();
            System.out.println("input flatting = ");
            flattingScan();
            coordinatesScan();
            prepareCoordinates();
            dataTransferObjectOut = dataController.getTransformedCoordinates(dataTransferObjectIn);
            System.out.println(dataTransferObjectOut);
        }
        catch(WrongInputDataFormatException e){
            System.out.println("illegal input! Please input float number");
            startConversion();
        }
    }

    private void majorAxisScan(){
        try{
            majorSemiAxis = Double.valueOf(ellipsoidScanner.nextLine());
        }
        catch (InputMismatchException | NumberFormatException ex){
            throw new WrongInputDataFormatException() ;
        }
    }
    private void flattingScan(){
        try{
            flatting = Double.valueOf(flattingScanner.nextLine());
        }
        catch (InputMismatchException | NumberFormatException ex){
            throw new WrongInputDataFormatException() ;
        }
    }
    private void coordinatesScan(){
        try{
            if (option == 1) {
                System.out.println("input X, [m] = ");
                firstCoordinate = Double.valueOf(coordinatesScanner.nextLine());
                System.out.println("input Y, [m] = ");
                secondCoordinate = Double.valueOf(coordinatesScanner.nextLine());
                System.out.println("input Z, [m] = ");
                thirdCoordinate = Double.valueOf(coordinatesScanner.nextLine());
            } else {
                System.out.println("input B, [degrees] = ");
                firstCoordinate = Double.valueOf(coordinatesScanner.nextLine());
                System.out.println("input L, [degrees] = ");
                secondCoordinate = Double.valueOf(coordinatesScanner.nextLine());
                System.out.println("input H, [m] = ");
                thirdCoordinate = Double.valueOf(coordinatesScanner.nextLine());
            }
        }
        catch (InputMismatchException | NumberFormatException ex){
            throw new WrongInputDataFormatException() ;
        }
    }
    private void prepareCoordinates() {
        dataTransferObjectIn.setCoordinateXorB(firstCoordinate);
        dataTransferObjectIn.setCoordinateYorL(secondCoordinate);
        dataTransferObjectIn.setCoordinateZorH(thirdCoordinate);
        dataTransferObjectIn.setSemiMajorAxis(majorSemiAxis);
        dataTransferObjectIn.setFlatting(flatting);
        dataTransferObjectIn.setOption(option);
    }
}
